#SAMtools docker image

This repository contains Dockerfile that packages SAMtools 1.2 Docker image.

SAMtools 1.2 can be downloaded [here](http://www.htslib.org/download/) and documentation can be found [here](http://www.htslib.org/doc/samtools.html).


### The recommended way to build an image is:

```
docker build -t samtools:1.2 .
```

### To run SAMtools 1.2 in container:
Let`s say we want to index sequence file with SAMtools.

If we have sequence file <name>.fasta in ~/work_dir/, following command will produce output index file <name>.fasta.fai in directory ~/work_dir/. 
```
docker run -v ~/work_dir:/mnt -u $(echo $UID) --rm samtools:1.2 faidx /mnt/<name>.fasta
```
